# tmux
inxi
~/.local/bin/fetching -e bars-xs

# screenfetch
# Enable the subsequent settings only in interactive sessions
case $- in
  *i*) ;;
    *) return;;
esac

cowthink "remember: if you are feeling like an impostor it means you have some degree of success in your life that you are attributing to luck"

/usr/bin/python3 ~/espeak.py

force_color_prompt=yes 

export BROWSER=/usr/bin/librewolf
# export PATH="~/miniconda3/bin:$PATH"  # commented out by conda initialize  # commented out by conda initialize

LD_LIBARAR_PATH=/usr/lib/wsl/lib/:$LD_LIBARAR_PATH

# time 
export TIMEFORMAT='r: %R, u: %U, s: %S'
export HISTTIMEFORMAT='%F %T'

# Path to your oh-my-bash installation.
export OSH=~/.oh-my-bash

# tere - alternative to cd and ls
tere() {
    local result=$(command tere "$@")
    [ -n "$result" ] && cd -- "$result"
}

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-bash is loaded.
OSH_THEME="powerline-naked"

# Uncomment the following line to use case-sensitive completion.

CASE_SENSITIVE="true"

# bash completion 
source /etc/profile.d/bash_completion.sh
bind 'set show-all-if-ambiguous on'
bind 'set completion-ignore-case on'
eval "$(register-python-argcomplete3 pipx)"
eval "$(zoxide init bash)"

export QT_STYLE_OVERRIDE=Adwaita-Dark

##########################
# ALIAS #
alias ssha='eval $(ssh-agent) && ssh-add'
alias ssh="kitty +kitten ssh"
alias mullvad-client="mullvad connect ; mullvad status -v"
alias lazygit="~/go/bin/lazygit"
alias mail-client="aerc"
alias matrix-client="~/Git/gomuks/gomuks"
alias signal-client="gurk"
alias signal-client-dir="cd ~/.local/share/gurk ; lsd"
alias jabber-client="profanity"
alias irc-client="irssi"
alias rss-client="newsboat"
alias podcast-client="castero"
alias torrent-client="aria2c"
alias mac-show="sudo macchanger -s wlp0s20f3"
alias mac-change="sudo ifconfig wlp0s20f3 down ; sudo macchanger -r wlp0s20f3"
alias mac-back="sudo macchanger -p wlp0s20f3 ; sudo ifconfig wlp0s20f3 up"
alias bluetooth-block="sudo rfkill block bluetooth"
alias bluetooth-unblock="sudo rfkill unblock bluetooth"
alias wifi-block="sudo rfkill block wifi"
alias wifi-unblock="sudo rfkill unblock wifi"
alias lsblk="lsblk -fi | grep -v loop"
alias video-ascii="bash ~/Bash/ascii-video.sh"
alias chess="~/.local/bin/chs"
alias gameoflife="~/Git/Game-of-Life/script/main.py"
alias bios-info="sudo dmidecode --type bios"
alias exif="exiftool -s"
alias sudo="doas"
alias de-conda="conda deactivate"
alias a-conda="conda activate"
alias webcam="mplayer tv:// -tv driver=v4l2:width=640:height=480:device=/dev/video3 -fps 15"
alias kabmat="kabmat"
alias ip="ip -c"
alias youtube-video="ytfzf -t"
alias youtube-audio="ytfzf -m"
alias calendar="when d; when c"
alias screenrecorder="wf-recorder --audio  -f ~/Videos/Recordings/'screen-record-$(date +%Y-%m-%d-%H-%M-%S).mp4'"
alias ip-wlp0s20f3="ip a s wlp0s20f3"
alias byebye="shutdown -h now"
alias mpvq="mpv --save-position-on-quit"
alias man="tldr"
alias rusted-ruins="cd ~/Git/rusted-ruins ; RUSTED_RUINS_ASSETS_DIR=./assets cargo run --release -p rusted-ruins"
alias hardware="python3 ~/hardware.py"
alias ps="procs -t"
alias alj-video="mpv --volume=80 https://invidious.sethforprivacy.com/watch?v=gCNeDWCI0vo"
alias alj-novideo="mpv --no-video --volume=80 https://invidious.sethforprivacy.com/watch?v=gCNeDWCI0vo"
alias dig="dog AAAA NS MX TXT"
alias benchmark="hyperfine --warmup 3"
alias musicdefcon="mpv --no-video --volume=35 https://somafm.com/defcon256.pls"
alias fx="~/go/bin/fx"
alias cmatrix="cmatrix -a -C  white"
alias irssi="/usr/local/bin/irssi"
alias find="fdfind"
alias tetris="tetris-thefenriswolf.tetris"
alias lls="exa -abghHliS"
alias lsd="lsd -l"
alias cava="cava -p ~/.config/cava/config"
alias cmus="cmus --show-cursor"
alias wifimon="sudo wavemon"
alias crypto="coinmon"
alias tomatoshell="tomatoshell -f -n 10 -t 30"
alias top="btm"
alias weather="curl wttr.in/Sheffield"
alias myip="wget -qO- http://ipecho.net/plain | xargs echo"
alias alacritty="env -u WAYLAND_DISPLAY alacritty"
alias tty-clock="tty-clock -cBrsb -C 7"
alias vim="nvim"
alias apt-update="doas nala update"
alias apt-upgrade="doas nala upgrade"
alias hasciicam="hasciicam -S 1 -I -c 10 -d /dev/video2"
alias cat="batcat"
alias ping="gping -n 0.2 -c gray"
alias pinglan="sh ~/Git/IPsweep/ipscan.sh"
alias df="duf"
alias ll="lsd"
alias ncdu="ncdu --color dark"
alias nnn="nnn -HUxde"
alias w3mduck="w3m https://www.duckduckgo.com" 
alias chatgpt="cd ~/cli-shellgpt ; python3 -m venv cli-shellgpt ; source cli-shellgpt/bin/activate"
alias swaylock="/usr/local/bin/swaylock --screenshots --clock --indicator --indicator-radius 100 --indicator-thickness 7 --effect-blur 7x5 --effect-vignette 0.5:0.5 --ring-color bb00cc --key-hl-color 880033 --line-color 00000000 --inside-color 00000088 --separator-color 00000000 --grace 2 --fade-in 0.2"

# EXPORT
export LS_COLORS="$(vivid generate dracula)"
export NNN_PLUG='f:finder;o:fzopen;p:mocplay;d:diffs;t:nmount;v:imgview'

#########################################################
# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"
##################################
# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"
##################################
# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_OSH_DAYS=13
##################################
# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"
##################################

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $OSH/custom?
# OSH_CUSTOM=/path/to/new-custom-folder

# To disable the uses of "sudo" by oh-my-bash, please set "false" to
# this variable.  The default behavior for the empty value is "true".
OMB_USE_SUDO=true

##################################

# Which completions would you like to load? (completions can be found in ~/.oh-my-bash/completions/*)
# Custom completions may be added to ~/.oh-my-bash/custom/completions/
# Example format: completions=(ssh git bundler gem pip pip3)
# Add wisely, as too many completions slow down shell startup.
completions=(
  git
  composer
  ssh
)

###################################

# Which aliases would you like to load? (aliases can be found in ~/.oh-my-bash/aliases/*)
# Custom aliases may be added to ~/.oh-my-bash/custom/aliases/
# Example format: aliases=(vagrant composer git-avh)
# Add wisely, as too many aliases slow down shell startup.
aliases=(
  general
)

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-bash/plugins/*)
# Custom plugins may be added to ~/.oh-my-bash/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  bashmarks
)

# Which plugins would you like to conditionally load? (plugins can be found in ~/.oh-my-bash/plugins/*)
# Custom plugins may be added to ~/.oh-my-bash/custom/plugins/
# Example format: 
#  if [ "$DISPLAY" ] || [ "$SSH" ]; then
#      plugins+=(tmux-autoattach)
#  fi

source "$OSH"/oh-my-bash.sh

# User configuration
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-bash libs,
# plugins, and themes. Aliases can be placed here, though oh-my-bash
# users are encouraged to define aliases within the OSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias bashconfig="mate ~/.bashrc"
# alias ohmybash="mate ~/.oh-my-bash"
. "$HOME/.cargo/env"
# source ~/Git/alacritty/extra/completions/alacritty.bash

# If not running interactively, don't do anything
case $- in
  *i*) ;;
    *) return;;
esac

# Path to the bash it configuration
export BASH_IT="~/.bash_it"

# Lock and Load a custom theme file.
# Leave empty to disable theming.
# location /.bash_it/themes/
export BASH_IT_THEME='bobby'

# Some themes can show whether `sudo` has a current token or not.
# Set `$THEME_CHECK_SUDO` to `true` to check every prompt:
#THEME_CHECK_SUDO='true'

# (Advanced): Change this to the name of your remote repo if you
# cloned bash-it with a remote other than origin such as `bash-it`.
# export BASH_IT_REMOTE='bash-it'

# (Advanced): Change this to the name of the main development branch if
# you renamed it or if it was changed for some reason
# export BASH_IT_DEVELOPMENT_BRANCH='master'

# Your place for hosting Git repos. I use this for private repos.
export GIT_HOSTING='git@git.domain.com'

# Don't check mail when opening terminal.
unset MAILCHECK

# Change this to your console based IRC client of choice.
export IRC_CLIENT='irssi'

# Set this to the command you use for todo.txt-cli
export TODO="t"

# Set this to the location of your work or project folders
#BASH_IT_PROJECT_PATHS="${HOME}/Projects:/Volumes/work/src"

# Set this to false to turn off version control status checking within the prompt for all themes
export SCM_CHECK=true
# Set to actual location of gitstatus directory if installed
#export SCM_GIT_GITSTATUS_DIR="$HOME/gitstatus"
# per default gitstatus uses 2 times as many threads as CPU cores, you can change this here if you must
#export GITSTATUS_NUM_THREADS=8

# Set Xterm/screen/Tmux title with only a short hostname.
# Uncomment this (or set SHORT_HOSTNAME to something else),
# Will otherwise fall back on $HOSTNAME.
#export SHORT_HOSTNAME=$(hostname -s)

# Set Xterm/screen/Tmux title with only a short username.
# Uncomment this (or set SHORT_USER to something else),
# Will otherwise fall back on $USER.
#export SHORT_USER=${USER:0:8}

# If your theme use command duration, uncomment this to
# enable display of last command duration.
#export BASH_IT_COMMAND_DURATION=true
# You can choose the minimum time in seconds before
# command duration is displayed.
#export COMMAND_DURATION_MIN_SECONDS=1

# Set Xterm/screen/Tmux title with shortened command and directory.
# Uncomment this to set.
#export SHORT_TERM_LINE=true

# Set vcprompt executable path for scm advance info in prompt (demula theme)
# https://github.com/djl/vcprompt
#export VCPROMPT_EXECUTABLE=~/.vcprompt/bin/vcprompt

# (Advanced): Uncomment this to make Bash-it reload itself automatically
# after enabling or disabling aliases, plugins, and completions.
# export BASH_IT_AUTOMATIC_RELOAD_AFTER_CONFIG_CHANGE=1

# Uncomment this to make Bash-it create alias reload.
# export BASH_IT_RELOAD_LEGACY=1

# Load Bash It
# source "$BASH_IT"/bash_it.sh
# export PATH="${PATH}:~/.cargo/bin/navi"

# TMATE Functions

TMATE_PAIR_NAME="$(whoami)-pair"
TMATE_SOCKET_LOCATION="/tmp/tmate-pair.sock"
TMATE_TMUX_SESSION="/tmp/tmate-tmux-session"

# Get current tmate connection url
tmate-url() {
  url="$(tmate -S $TMATE_SOCKET_LOCATION display -p '#{tmate_ssh}')"
  echo "$url" | tr -d '\n' | pbcopy
  echo "Copied tmate url for $TMATE_PAIR_NAME:"
  echo "$url"
}



# Start a new tmate pair session if one doesn't already exist
# If creating a new session, the first argument can be an existing TMUX session to connect to automatically
tmate-pair() {
  if [ ! -e "$TMATE_SOCKET_LOCATION" ]; then
    tmate -S "$TMATE_SOCKET_LOCATION" -f "$HOME/.tmate.conf" new-session -d -s "$TMATE_PAIR_NAME"

    while [ -z "$url" ]; do
      url="$(tmate -S $TMATE_SOCKET_LOCATION display -p '#{tmate_ssh}')"
    done
    tmate-url
    sleep 1

    if [ -n "$1" ]; then
      echo $1 > $TMATE_TMUX_SESSION
      tmate -S "$TMATE_SOCKET_LOCATION" send -t "$TMATE_PAIR_NAME" "TMUX='' tmux attach-session -t $1" ENTER
    fi
  fi
  tmate -S "$TMATE_SOCKET_LOCATION" attach-session -t "$TMATE_PAIR_NAME"
}



# Close the pair because security
tmate-unpair() {
  if [ -e "$TMATE_SOCKET_LOCATION" ]; then
    if [ -e "$TMATE_SOCKET_LOCATION" ]; then
      tmux detach -s $(cat $TMATE_TMUX_SESSION)
      rm -f $TMATE_TMUX_SESSION
    fi

    tmate -S "$TMATE_SOCKET_LOCATION" kill-session -t "$TMATE_PAIR_NAME"
    echo "Killed session $TMATE_PAIR_NAME"
  else
    echo "Session already killed"
  fi 
}

# colours maybe you want
PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\]\w\[\033[1;31m\]\$\[\033[0m\] '

# Created by `pipx` on 2022-11-13 10:31:10
export PATH="$PATH:~/.local/bin"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('~/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "~/miniconda3/etc/profile.d/conda.sh" ]; then
        . "~/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="~/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

