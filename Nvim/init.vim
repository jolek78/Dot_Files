autocmd VimEnter * wincmd p

nnoremap H gT
nnoremap L gt

packloadall

call plug#begin()
  Plug 'neovim/nvim-lspconfig'
  Plug 'masukomi/vim-markdown-folding', { 'for': 'markdown' }
  Plug 'Exafunction/codeium.vim'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}  
  Plug 'chesleytan/wordcount.vim'
  Plug 'easymotion/vim-easymotion'
  Plug 'vim-scripts/indentpython.vim'
  Plug 'prettier/vim-prettier', { 'do': 'yarn install --frozen-lockfile --production' }
  Plug 'nvie/vim-flake8'
  Plug 'lervag/vimtex'
  Plug 'kien/ctrlp.vim'
  Plug 'tpope/vim-fugitive'
  Plug 'tpope/vim-commentary'
  Plug 'benmills/vimux'
  Plug 'mfussenegger/nvim-dap'          " to install python debug
  Plug 'nvim-lualine/lualine.nvim'
  Plug 'kyazdani42/nvim-web-devicons'
  Plug 'arcticicestudio/nord-vim'
  Plug 'nvie/vim-togglemouse'
  Plug 'albertz/vim-mouseclick'
  Plug 'preservim/nerdtree'
  Plug 'Xuyuanp/nerdtree-git-plugin'
  Plug 'ryanoasis/vim-devicons'
  Plug 'pearofducks/ansible-vim'
  Plug 'mattn/emmet-vim'
  Plug 'bagrat/vim-buffet'
  Plug 'reedes/vim-wheel'
  Plug 'anschnapp/move-less'
  Plug 'lepture/vim-jinja'
  Plug 'honza/vim-snippets'
  Plug 'preservim/nerdtree'
  Plug 'roxma/nvim-yarp'
  Plug 'webdevel/tabulous'
  Plug 'pacha/vem-tabline'
  Plug 'vim-utils/vim-husk'
  Plug 'sharkdp/bat'
  Plug 'vim-scripts/DrawIt'
  Plug 'Yggdroot/indentLine'
  Plug 'nvie/vim-togglemouse'
  Plug 'pearofducks/ansible-vim'
  Plug 'mattn/emmet-vim'
  Plug 'anschnapp/move-less'
  Plug 'philrunninger/nerdtree-visual-selection'
  Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
  Plug 'tpope/vim-eunuch'
  Plug 'jiangmiao/auto-pairs'
  Plug 'christoomey/vim-system-copy'
  Plug 'tmux-plugins/vim-tmux-focus-events'
  Plug 'christoomey/vim-tmux-navigator'
  Plug 'hattya/python-indent.vim', { 'for': 'python' }
  Plug 'vimwiki/vimwiki'
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'tpope/vim-fugitive'
  Plug 'junegunn/gv.vim'
call plug#end()

set mouse=a

autocmd FileType yaml setlocal ai et ts=2 sts=2 sw=2 cul expandtab

set encoding=UTF-8
set nocompatible

filetype off

autocmd FileType apache setlocal commentstring=#\ %s

" call deoplete#custom#var('tabnine', {
" \ 'line_limit': 500,
" \ 'max_num_results': 20,
" \ })

" set rtp+=~/.vim/bundle/Vundle.vim
" call vundle#begin()
" Plugin 'Valloric/YouCompleteMe'
" Plugin 'Yggdroot/indentLine'
" call vundle#end()

filetype plugin indent on
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab


let g:indentLine_char_list = ['|', '¦', '┆', '┊']
let g:indentLine_setColors = 2

set hidden
" autocmd BufEnter * call ncm2#enable_for_buffer()
autocmd vimenter * if !argc() | NERDTree | endif
autocmd VimEnter * NERDTree | wincmd p

set completeopt=noinsert,menuone,noselect

let NERDTreeShowHidden=1
let g:airline_powerline_fonts = 1
let g:vimfiler_as_default_explorer = 1
let g:NERDTreeGitStatusUseNerdFonts = 1

let g:webdevicons_enable = 1
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_enable_unite = 1
let g:webdevicons_enable_vimfiler = 1
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_airline_statusline = 1
let g:webdevicons_enable_ctrlp = 1
let g:webdevicons_enable_flagship_statusline = 1
let g:WebDevIconsUnicodeDecorateFileNodes = 1
let g:WebDevIconsUnicodeGlyphDoubleWidth = 1

lua << END
require('lualine').setup()
options = { theme = 'gruvbox' }
END

let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
nmap s <Plug>(easymotion-overwin-f)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
nmap s <Plug>(easymotion-overwin-f2)

" Turn on case-insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)


au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |

" Virtualenv support
" py3 << EOF
" import os
" import sys
" if 'VIRTUAL_ENV' in os.environ:
"  project_base_dir = os.environ['VIRTUAL_ENV']
"  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"  exec(compile(open(activate_this, "rb").read(), activate_this, 'exec'), dict(__file__=activate_this))
" EOF

" use <tab> to trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()


" Code action on <leader>a
vmap <leader>a <Plug>(coc-codeaction-selected)<CR>
nmap <leader>a <Plug>(coc-codeaction-selected)<CR>

" Format action on <leader>f
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
" Goto definition
nmap <silent> gd <Plug>(coc-definition)
" Open definition in a split window
nmap <silent> gv :vsp<CR><Plug>(coc-definition)<C-W>L

let python_highlight_all=1
syntax on


nnoremap <F2> :set invpaste paste?<CR>

nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <C-Up> :tabfirst<CR>
nnoremap <C-Down> :tablast<CR>

set pastetoggle=<F2>
set showmode

filetype indent plugin on
filetype plugin on

let g:vimtex_view_method = 'zathura'
let maplocalleader = ","

set number
set ruler

syntax on
